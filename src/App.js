import logo from './logo.svg';
import React ,{useState} from "react";
import './App.css';
import ButtonSide from "./Components/Buttons/ButtonSide";
import UserSide from "./Components/Users/UserSide";

const App = () =>{
    const [users, setUsers] = useState([]);
    const [toggleMill, setToggleMill] =useState(true);
    const [totalWealth,setTotalWealth]=useState(0);
    const [showTotalWealth,setShowTotalWealth]=useState(false);

    return(
      <div className="container">
          <ButtonSide
              users={users}
              setUsers={setUsers}
              toggleMill={toggleMill}
              setToggleMill={setToggleMill}
              setTotalWealth={setTotalWealth}
              setShowTotalWealth={setShowTotalWealth}
          ></ButtonSide>
          <UserSide
              users={users}
              totalWealth={totalWealth}
              showTotalWealth={showTotalWealth}
          ></UserSide>
      </div>
    );
}

export default App;
