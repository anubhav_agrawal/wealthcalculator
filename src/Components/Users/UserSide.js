import React from "react";
import DisplayUsers from "./DisplayUsers";
import ShowTotalWealth from "./ShowTotalWealth";

const UserSide = (props)=>{
    return(
        <div className="rightSide">
            <div className="heading">
                <h2>Person</h2>
                <h2>Wealth</h2>
            </div>
            {(props.users.length >0) && <DisplayUsers users={props.users}/>}
            {props.showTotalWealth && <ShowTotalWealth totalWealth={props.totalWealth}/>}
        </div>
    )
}

export default UserSide;