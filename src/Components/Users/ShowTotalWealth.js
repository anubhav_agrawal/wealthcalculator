import React from "react";

const ShowTotalWealth=(props)=>{
    return(
        <div className="wealthShow">
            <div>Total Wealth: </div>
            <div>{`${props.totalWealth}`}</div>
        </div>
    );
}

export default ShowTotalWealth;