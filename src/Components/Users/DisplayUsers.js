import React from "react";

const DisplayUsers = (props)=>{
    return(
        props.users.map((user)=>{
            if(user.onScreen)
            return(
                <div className="displayData">
                    <div>{user.name}</div>
                    <div>{`${user.wealth}`}</div>
                </div>
            )
        })
    );
}

export default DisplayUsers;