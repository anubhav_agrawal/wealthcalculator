import React from "react";

const Button =(props)=>{
    return(
        <button
            id={props.id}
            children={props.description}
            onClick={props.action}
            className="buttonStyle"
        />
    )
}

export default React.memo(Button);