import React from "react";
import Button from "./Button";


const ButtonSide =(props)=>{
    const {users,setUsers,toggleMill,setToggleMill,setTotalWealth,setShowTotalWealth} = props;


    const getRandomUser=async () => {
        setShowTotalWealth(false);

        const url = 'https://randomuser.me/api';
        const response = await fetch(url);
        const result = await response.json();
        const name = result['results'][0]['name']['first']
        const wealth = Math.floor(Math.random()*1e6);
        const user = {"name":name,"wealth":wealth,"onScreen":true};
        setUsers((prevUsers)=>{
            return [...prevUsers,user];
        });
    }

    const doubleMoney=()=>{
        setShowTotalWealth(false);

        const newUsers= users.map((user) =>({
            ...user,
            wealth: user.wealth*2,
        }));
        //users = users.map()
        //console.log(users);
        setUsers(newUsers);
    }

    const showMill=()=>{
        setShowTotalWealth(false);

        if(toggleMill) {
            const millArr = users.filter(user => {
                return (user.wealth >= 1e6 ? true:false);
            });
            //const displayMill = millArr.map(mil)
            setUsers(millArr);
            setToggleMill(false);
        }
        else {
            setUsers(users);
            setToggleMill(true);
        }
    }

    const sortRich=()=>{
        const sortedUsers =[...users].sort((a,b) => {
            return (b.wealth - a.wealth);
        });
        setUsers(sortedUsers);
    }

    const calculateWealth=()=>{
        /*let total_wealth=0;
        users.forEach(user =>{
            total_wealth += user.wealth;
        })*/
        const total = users.reduce((total_sum,user)=>total_sum+user.wealth,0);
        //console.log(total);
        setTotalWealth(total);
        setShowTotalWealth(true);
    }

    return(
        <div className="leftSide">
            <Button
                id="add-user"
                description = "Add User"
                action={getRandomUser}
            />
            <Button
                id="double-money"
                description = "Double Money"
                action={doubleMoney}
            />
            <Button
                id="show-mill"
                description = "Show Only Millionaires"
                action={showMill}
            />
            <Button
                id="sort-rich"
                description = "Sort By Richest"
                action={sortRich}
            />
            <Button
                id="calculate-wealth"
                description = "Calculate Entire Wealth"
                action={calculateWealth}
            />
        </div>
    )
}

export default ButtonSide;